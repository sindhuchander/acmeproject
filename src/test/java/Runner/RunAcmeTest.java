package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/Features/AcmeFeatures.feature", 
glue="steps", 
plugin= {"pretty"},
monochrome=true, 
dryRun = false,
snippets=SnippetType.CAMELCASE
)
/*dryRun = true,
snippets=SnippetType.CAMELCASE*/

public class RunAcmeTest {


}
