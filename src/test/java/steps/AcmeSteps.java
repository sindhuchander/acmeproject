package steps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AcmeSteps {

	public static ChromeDriver driver;
	public static Actions actions;
	
	@Given ("Open AcmeBrowser")
	public void OpenAcmeBrowser()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@And ("Max AcmeBrowser")
	public void MaxAcmeBrowser()
	{
		driver.manage().window().maximize();
	}
	
	@And ("Enter AcmeUrl")
	public void EnterAcmeURL()
	{
		driver.get("https://acme-test.uipath.com/account/loginManual");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@And ("Enter AcmeEmail")
	public void EnterAcmeEmail()
	{
		driver.findElementById("email").sendKeys("sindhuchander@gmail.com");
	}
	
	@And ("Enter AcmePword")
	public void EnterAcmePwd()
	{
		driver.findElementById("password").sendKeys("Sindhu2019");
	}
	
	@And ("Click on AcmeSignon")
	public void ClickAcmeSignOn()
	{
		driver.findElementById("buttonLogin").click();
	}
	
	@Given("HoverVendors")
	public void hovervendors() {
		actions= new Actions(driver);
		WebElement eleVendor = driver.findElementByXPath("//button[text()=' Vendors']");
		actions.click(eleVendor).perform();
		
	}

	@And("searchVendor")
	public void searchvendor() {
		actions= new Actions(driver);
		WebElement eleSearchVendor = driver.findElementByXPath("//*[text()='Search for Vendor']");
		actions.click(eleSearchVendor).perform();
	}

	@And("enterKey")
	public void enterkey() {
		driver.findElementById("vendorTaxID").sendKeys("IT754893");
	}

	@When("clickSearch")
	public void clicksearch() {
		driver.findElementById("buttonSearch").click();
	}

	@Then("readText")
	public void readtext() {
		WebElement eleTable = driver.findElementByXPath("//table[@class = 'table']");
			List<WebElement> columns = eleTable.findElements(By.tagName("td"));
			
			String textName = columns.get(0).getText();
			System.out.println(textName);

}
}
