package testcases;

import org.testng.annotations.Test;

import ProjPackage.acmeProj;
import pages.LoginPage;

public class ProjectTC extends acmeProj{
	
@Test
	public void TestCase1()
	{
		new LoginPage()
		.enterEmail()
		.enterPwd()
		.clickSignON()
		.hoverVendors()
		.searchVendor()
		.enterKey()
		.clickSearch()
		.readText();
	}

}
