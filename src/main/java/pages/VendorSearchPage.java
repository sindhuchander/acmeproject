package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import ProjPackage.acmeProj;

public class VendorSearchPage extends acmeProj {

	public VendorSearchPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "vendorTaxID") WebElement eleVendorKey;
	public VendorSearchPage enterKey()
	{
		eleVendorKey.sendKeys("IT754893");
		return this;
	}
	
	@FindBy(how = How.ID, using="buttonSearch") WebElement eleSearchButton;
	public ResultsPage clickSearch()
	{
		eleSearchButton.click();
		return new ResultsPage();
		//driver.findElementById("buttonSearch").click();
	}
}
