package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import ProjPackage.acmeProj;

public class ResultsPage extends acmeProj {

	public ResultsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//table[@class = 'table']") WebElement eleTable;
	public void readText()
	{
		List<WebElement> columns = eleTable.findElements(By.tagName("td"));
		String textName = columns.get(0).getText();
		System.out.println(textName);
	}
	
}
