package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import ProjPackage.acmeProj;

public class LoginPage extends acmeProj {
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "email") WebElement eleEmail;
	public LoginPage enterEmail()
	{
		eleEmail.sendKeys("sindhuchander@gmail.com");
		return this;
	}
	
	@FindBy(how = How.ID, using = "password") WebElement elePwd;
	public LoginPage enterPwd()
	{
		elePwd.sendKeys("Sindhu2019");
		return this;
	}
	
	@FindBy(how = How.ID, using = "buttonLogin") WebElement eleSignOn;
	public DashboardPage clickSignON()
	{
		eleSignOn.click();
		return new DashboardPage();

	}


}
