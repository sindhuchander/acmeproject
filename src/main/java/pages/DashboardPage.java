package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import ProjPackage.acmeProj;

public class DashboardPage extends acmeProj {
	
	public static Actions actions;

	public DashboardPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button[text()=' Vendors']") WebElement eleVendors;
	public DashboardPage hoverVendors()
	{
		actions = new Actions(driver);
		//actions.moveToElement(eleVendor).click(eleSearchVendor).perform();
		actions.click(eleVendors).perform();
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "//*[text()='Search for Vendor']") WebElement eleSearchVendors;
	public VendorSearchPage searchVendor()
	{
		actions.click(eleSearchVendors).perform();
		return new VendorSearchPage();
	}
	
}



